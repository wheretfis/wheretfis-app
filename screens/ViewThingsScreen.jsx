import * as React from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, ScrollView, TextInput, Alert, Image } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import * as Utils from '../utils';
import { styles } from '../style';
import Checkbox from 'expo-checkbox';
import { HighlightSpanKind } from 'typescript';

export function ViewThingScreen({ navigation, route }) {
    const [loading, setLoading] = React.useState(false);
    const [id, setId] = React.useState(null);
    const [parent, setParent] = React.useState("");
    const [description, setDescription] = React.useState("");
    const [notes, setNotes] = React.useState("");
    const [isContainer, setIsContainer] = React.useState(false);
    const [isLocation, setIsLocation] = React.useState(false);
    const [photo, setPhoto] = React.useState(null);
    const [tagList, setTagList] = React.useState([]);

    function scanId() {
        navigation.navigate('scanner', {
            callingScreen: "view-thing",
        });
    }

    function getThing(assetTag) {
        // We need to make an API call to get the item fields.
        setLoading(true);

        Utils.apiCall(`/api/items/${assetTag}`).then((response) => {
            setId(response.id);

            setDescription(response.description);
            //navigation.setOptions({title: response.description});

            setNotes(response.notes);
            setIsLocation(response.is_physical_place);
            setIsContainer(response.is_container);
            setParent(response.parent_container);
            if (response.picture_data != null) {
                setPhoto(response.picture_data);
            }
            setTagList(response.asset_tags);
            setLoading(false);
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            // when the screen is focused

            if (route.params?.scanResult) {
                // This is called if scanResult is not null, and presumably we just got done with the scanner.
                setLoading(true);

                var rawScanResult = route.params.scanResult.toString();
                // If the scanned QR code has a "/" in it, then it's likely that this is a full URL that includes an ID as a link (in the hopes that you use their services from buying their asset tags). We just need to extract the unique ID, which is normally the last part of the URL, so iterate on it until no "/"s remain.
                // Also you might wonder why this isn't in the Utils file, and there's a bug in React Native where native types like strings aren't correctly parsed, so we have to do it this stupid way.
                while (rawScanResult.includes("/")) {
                    var splits = rawScanResult.split("/");
                    rawScanResult = splits[1];
                }
                getThing(rawScanResult);
            }
            else {
                if (route.params?.tagId) {
                    // This is normally passed in via route parameters
                    getThing(route.params.tagId);
                }
                else if (description === "") {
                    // This means we have no idea what thing we should be loading, scan something.
                    scanId();
                }
            }

            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                setLoading(false);
            };
        }, [route.params?.scanResult, route.params?.tagId])
    );

    return (
        <View style={styles.container}>
            {loading && <View style={styles.center}>
                <ActivityIndicator size="large" color="#4a4a4a" ></ActivityIndicator>
            </View>}

            {!loading && <View>
                <ScrollView>
                    <Text style={styles.textBig}>{description}</Text>

                    {photo &&
                        <View>
                            <Image
                                style={styles.oneOneRatio}
                                source={{
                                    uri: photo,
                                }}>
                            </Image>
                        </View>
                    }

                    <Text style={styles.textSmall}>{notes}</Text>

                    <View style={styles.section}>
                        <Checkbox
                            style={styles.checkbox}
                            value={isContainer}
                            disabled="true"
                            color={isContainer ? '#4a4a4a' : undefined}
                        />
                        <Text style={styles.labelText}>Container</Text>
                    </View>

                    <View style={styles.section}>
                        <Checkbox
                            section
                            value={isLocation}
                            disabled="true"
                            color={isLocation ? '#4a4a4a' : undefined}
                        />
                        <Text style={styles.labelText}>Location</Text>
                    </View>

                    <Text style={styles.labelText}>Asset Tags:</Text>
                    <View style={styles}>
                        {tagList.length > 0 && tagList.map((tag, index) => (
                            <View>
                                <TouchableOpacity
                                    key={tag}
                                    style={[styles.tagButton]}>
                                    
                                    <Text style={styles.tagButtonText} key={tag}>[{tag}]</Text>
                                </TouchableOpacity>
                            </View>
                        ))}
                        {
                            tagList.length === 0 &&
                            <View style={styles.tagNA}>
                                <Text style={styles.tagButtonText}>N/A</Text>
                            </View>
                        }
                    </View>

                    {parent != null &&
                        <View>
                            <Text style={styles.labelText}>In Container:</Text>
                            <TouchableOpacity
                                style={[styles.buttonParent]}
                                onPress={() => navigation.push('view-thing', { tagId: parent.asset_tags[0] })}>
                                <Text style={styles.buttonParentText}>
                                    <Ionicons name="return-up-back" size={20} color="#4a4a4a" /> {parent.description}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }

                    <Text style={styles.labelText}>Actions:</Text>
                    <TouchableOpacity
                        style={[styles.button]}
                        onPress={() => navigation.navigate('edit-thing', { id: id, description: description, notes: notes, isContainer: isContainer, isLocation: isLocation, parent: parent, photo: photo, tagList: tagList })}>
                        <Text style={styles.buttonText}>
                            <Ionicons name="pencil" size={20} color="#ffffff" /> Edit Thing
                        </Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>}
        </View>
    );
}
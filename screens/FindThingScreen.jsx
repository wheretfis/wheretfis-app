import * as React from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, ScrollView, TextInput, Alert, Image } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import * as Utils from '../utils';
import { styles } from '../style';
import Checkbox from 'expo-checkbox';

export function FindThingScreen({ navigation, route }) {
    const [loading, setLoading] = React.useState(false);
    const [searchTerm, setSearchTerm] = React.useState("");
    const [items, setItems] = React.useState([]);

    function findThing() {
        // We need to make an API call to get the item fields.
        setLoading(true);

        Utils.apiCall(`/api/items/`, "GET", { q: searchTerm }).then((response) => {
            setItems(response)
            setLoading(false);
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                setLoading(false);
            };
        }, [])
    );

    return (
        <View style={styles.container}>
            {loading && <View style={styles.center}>
                <ActivityIndicator size="large" color="#4a4a4a" ></ActivityIndicator>
            </View>}

            {!loading && <View>
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        onChangeText={text => {
                            setSearchTerm(text)
                        }}
                        value={searchTerm}
                        placeholder="" />

                    <TouchableOpacity
                        style={[styles.button]}
                        onPress={() => findThing()}>
                        <Text style={styles.buttonText}>
                            <Ionicons name="search" size={20} color="#ffffff" /> Find Thing
                        </Text>
                    </TouchableOpacity>

                    <View>
                        <Text style={styles.labelText}>Results:</Text>
                        <View style={styles}>
                            {items.length > 0 && items.map((item, index) => (
                                <View>
                                    <TouchableOpacity
                                        style={[styles.buttonParent]}
                                        key={item.id}
                                        onPress={() => navigation.push('view-thing', { tagId: item.asset_tags[0] })}>
                                        <Text style={styles.buttonParentText} key={item.id}>{item.is_container && !item.is_physical_place && <Ionicons name="archive" size={20} color="#4a4a4a"/>}{item.is_physical_place && <Ionicons name="home" size={20} color="#4a4a4a"/>} {item.description}</Text>
                                    </TouchableOpacity>
                                </View>
                            ))}
                            {
                                items.length === 0 &&
                                <View style={styles.tagNA}>
                                    <Text style={styles.tagButtonText}>N/A</Text>
                                </View>
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>}
        </View>
    );
}
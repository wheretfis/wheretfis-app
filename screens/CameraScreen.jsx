import * as React from 'react';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { StyleSheet, View, Text, TouchableOpacity, Vibration, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { styles } from '../style';
import { Camera } from 'expo-camera';

export function CameraScreen({ navigation, route }) {
    const [showHomeButton, setShowHomeButton] = React.useState(true);
    const [hasCameraPermission, setHasCameraPermission] = React.useState(null);
    const [camera, setCamera] = React.useState(null);
    //const [image, setImage] = React.useState(null);
    const [type, setType] = React.useState(Camera.Constants.Type.back);

    React.useEffect(() => {
        (async () => {
            const { status } = await Camera.requestCameraPermissionsAsync();
            setHasCameraPermission(status === 'granted');

            if (route.params?.cancelButton) {
                setShowHomeButton(true);
            }
        })();
    }, []);

    const takePicture = async () => {
        if (camera) {
            const data = await camera.takePictureAsync({base64: true})
            finishScan("data:image/jpg;base64," + data.base64);
        }
    }

    function finishScan(image, gotoOverride=null) {
        var options = {
            name: route.params?.callingScreen ? route.params?.callingScreen : "home",
            params: { photo: image },
            merge: true,
        };
        if(gotoOverride){
            options.name = gotoOverride;
        }
        navigation.navigate(options);
    }

    if (hasCameraPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={styles.container}>
            <Camera
                type={type}
                style={styles.oneOneRatio}
                ref={ref => setCamera(ref)}
            />

            <View style={styles.endContainer}>
                 <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => takePicture()}>
                        
                    <Text style={[styles.buttonText]}>
                        <Ionicons name="camera" size={20} color="#ffffff" /> Take Photo
                    </Text>
                </TouchableOpacity>

                {showHomeButton && <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.goBack()}>

                    <Text style={[styles.buttonText]}>
                        <Ionicons name="close" size={20} color="#ffffff" /> Cancel
                    </Text>
                </TouchableOpacity>}
            </View>
        </View >

    );
}

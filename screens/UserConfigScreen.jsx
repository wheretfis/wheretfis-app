import * as React from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, TextInput, Alert, ScrollView } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import Spinner from 'react-native-loading-spinner-overlay';
import * as Utils from '../utils';
import { styles } from '../style';

export function UserConfigScreen({ navigation, route }) {
    const [loading, setLoading] = React.useState(false);
    const [id, setId] = React.useState("");
    const [userName, setUserName] = React.useState("");

    function SaveConfig() {
        setLoading(true);
        var data = {
            full_name: userName,
        };
        Utils.apiCall(`/api/users/${id}`, "PUT", data).then((response) => {
            Alert.alert("Success", "User info saved.");
            setLoading(false);
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            // when the screen is focused
            setLoading(true);
            Utils.apiCall("/api/users/me").then((response) => {
                setUserName(response.full_name);
                setId(response.id);
                setLoading(false);
            });

            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                setLoading(false);
            };
        }, [])
    );

    return (
        <View style={styles.container}>
            <ScrollView>
                {loading && <View style={styles.center}>
                    <ActivityIndicator size="large" color="#4a4a4a" ></ActivityIndicator>
                </View>}

                {!loading && <View>
                    <Text style={styles.labelText}>Name:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={text => {
                            setUserName(text)
                        }}
                        value={userName}
                        placeholder="John Doe" />
                    <TouchableOpacity
                        style={[styles.button]}
                        onPress={() => SaveConfig()}>
                        <Text style={styles.buttonText}>
                            <Ionicons name="person" size={20} color="#ffffff" /> Save
                        </Text>
                    </TouchableOpacity>
                </View>}
            </ScrollView>
        </View>
    );
}
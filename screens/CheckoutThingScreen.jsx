import * as React from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, ScrollView, TextInput, Alert, Switch } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import * as Utils from '../utils';
import { styles } from '../style';
import Checkbox from 'expo-checkbox';

export function CheckoutThingScreen({ navigation, route }) {
    const [id, setId] = React.useState("");

    function ScanId() {
        var options = {
            callingScreen: "checkout-thing",
            cancelButton: true,
        };
        navigation.navigate('scanner', options);
    }

    useFocusEffect(
        React.useCallback(() => {
            // when the screen is focused
            if (route.params?.scanResult || route.params?.scanResult === "") {
                // This is called if scanResult is not null, and presumably we just got done with the scanner.
                var rawScanResult = route.params.scanResult.toString();
                // If the scanned QR code has a "/" in it, then it's likely that this is a full URL that includes an ID as a link (in the hopes that you use their services from buying their asset tags). We just need to extract the unique ID, which is normally the last part of the URL, so iterate on it until no "/"s remain.
                // Also you might wonder why this isn't in the Utils file, and there's a bug in React Native where native types like strings aren't correctly parsed, so we have to do it this stupid way.
                while (rawScanResult.includes("/")) {
                    var splits = rawScanResult.split("/");
                    rawScanResult = splits[1];
                }

                // This was a scan for the item ID.
                setId(rawScanResult);

                // We need to make an API call to get the item fields.
                var data = {
                    parent_container: null
                };

                Utils.apiCall(`/api/items/${rawScanResult}/update`, "POST", data).then((response) => {
                    Alert.alert("Success", `'${response.description}' checked out.`);
                    navigation.goBack();
                });
            }
            else {
                ScanId();
            }

            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [route.params?.scanResult])
    );

    return (
        <View style={styles.container}>
            <View style={styles.center}>
                <ActivityIndicator size="large" color="#4a4a4a" ></ActivityIndicator>
            </View>
        </View>
    );
}
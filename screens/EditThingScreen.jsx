import * as React from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, ScrollView, TextInput, Alert, Image } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import * as Utils from '../utils';
import { styles } from '../style';
import Checkbox from 'expo-checkbox';

export function EditThingScreen({ navigation, route }) {
    const [loading, setLoading] = React.useState(false);
    const [id, setId] = React.useState(null);
    const [parent, setParent] = React.useState("");
    const [description, setDescription] = React.useState("");
    const [notes, setNotes] = React.useState("");
    const [isContainer, setIsContainer] = React.useState(false);
    const [isLocation, setIsLocation] = React.useState(false);
    const [photo, setPhoto] = React.useState(null);
    const [tagList, setTagList] = React.useState([]);
    const [isNew, setIsNew] = React.useState(false);

    function scanId() {
        navigation.navigate('scanner', {
            callingScreen: "edit-thing",
        });
    }

    function takePhoto() {
        navigation.navigate('camera', {
            callingScreen: "edit-thing",
        });
    }

    function createThing() {
        if (description == undefined || description === "") {
            Alert.alert("Error", "You must provide a Thing description.");
        }
        else if (tagList.length === 0) {
            Alert.alert("Error", "You must scan at least one Asset Tag for the Thing.");
        }
        else {
            setLoading(true);
            var data = {
                description: description,
                notes: notes,
                is_container: isContainer,
                is_physical_place: isLocation,
                asset_tags: tagList,
            };
            if (photo) {
                data.picture_data = photo;
            }
            Utils.apiCall("/api/items", "POST", data).then((response) => {
                Alert.alert("Success", `'${response.description}' was registered!`);
                navigation.goBack();
            }).finally(() => setLoading(false));
        }
    }

    function updateThing() {
        if (description == undefined || description === "") {
            Alert.alert("Error", "You must provide a Thing description.");
        }
        else if (tagList.length === 0) {
            Alert.alert("Error", "You must scan at least one Asset Tag for the Thing.");
        }
        else {
            setLoading(true);
            var data = {
                description: description,
                notes: notes,
                picture_data: photo,
            };
            if (photo === null) {
                data.picture_data = ""
            }
            Utils.apiCall(`/api/items/${tagList[0]}`, "PUT", data).then((response) => {
                Alert.alert("Success", "Item info saved.");
                setLoading(false);
            });
        }

    }


    function deleteThing() {
        Alert.alert('Delete Thing', `Are you sure you want to delete '${description}'?`, [
            {
                text: 'Cancel',
                onPress: () => { },
                style: 'cancel',
            },
            {
                text: 'Confirm',
                onPress: () => {
                    setLoading(true);
                    Utils.apiCall(`/api/items/${id}`, "DELETE").then(() => {
                        Alert.alert("Success", `'${description}' deleted.`);
                    }).finally(() => {
                        setLoading(false);
                        navigation.goBack();
                    });
                },
                style: 'destructive',
            }
        ]);
    }


    function removeTag(tag) {
        const newList = tagList.filter((item) => item !== tag);
        setTagList(newList);
    }

    useFocusEffect(
        React.useCallback(() => {
            // when the screen is focused
            if (route.params?.scanResult) {
                // This is called if scanResult is not null, and presumably we just got done with the scanner.
                var rawScanResult = route.params.scanResult.toString();
                // If the scanned QR code has a "/" in it, then it's likely that this is a full URL that includes an ID as a link (in the hopes that you use their services from buying their asset tags). We just need to extract the unique ID, which is normally the last part of the URL, so iterate on it until no "/"s remain.
                // Also you might wonder why this isn't in the Utils file, and there's a bug in React Native where native types like strings aren't correctly parsed, so we have to do it this stupid way.
                while (rawScanResult.includes("/")) {
                    var splits = rawScanResult.split("/");
                    rawScanResult = splits[1];
                }

                //setId(rawScanResult);
                if (tagList.indexOf(rawScanResult) === -1) {
                    // Only add the new tag if it doesn't already exist in the list.
                    setTagList(tagList.concat([rawScanResult]));
                }
            }
            else {
                // newItem will be set if we are not editing an existing item, but instead creating a new one.
                if (route.params?.newItem) {
                    setIsNew(true);
                }
                else if (route.params?.id) {
                    // Here, we're passing in all the item info already, no need to grab it again.
                    setId(route.params.id);
                    setDescription(route.params.description);
                    setNotes(route.params.notes);
                    setIsContainer(route.params.isContainer);
                    setIsLocation(route.params.isLocation);
                    setParent(route.params.parent);
                    setPhoto(route.params.photo);
                    setTagList(route.params.tagList);
                }
                else {
                    // If newItem is not set and we have no ID passed in, then we need to scan one to edit.
                    scanId();
                }

            }

            if (route.params?.photo) {
                setPhoto(route.params.photo);
            }

            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                setLoading(false);
            };
        }, [route.params?.photo, route.params?.scanResult])
    );

    return (
        <View style={styles.container}>
            {loading && <View style={styles.center}>
                <ActivityIndicator size="large" color="#4a4a4a" ></ActivityIndicator>
            </View>}

            {!loading && <View>
                <ScrollView>
                    <Text style={styles.labelText}>Description:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={text => {
                            setDescription(text)
                        }}
                        value={description}
                        placeholder="Description or name of the Thing" />

                    {photo &&
                        <View>
                            <Image
                                style={styles.oneOneRatio}
                                source={{
                                    uri: photo,
                                }}>
                            </Image>
                        </View>
                    }

                    <Text style={styles.labelText}>Notes:</Text>
                    <TextInput
                        style={styles.largeInput}
                        multiline={true}
                        numberOfLines={3}
                        onChangeText={text => {
                            setNotes(text)
                        }}
                        value={notes}
                        placeholder="Any extra notes to keep about the Thing" />

                    <View style={styles.section}>
                        <Checkbox
                            style={styles.checkbox}
                            value={isContainer}
                            onValueChange={setIsContainer}
                            color={isContainer ? '#4a4a4a' : undefined}
                        />
                        <Text style={styles.labelText}>Container</Text>
                    </View>

                    <View style={styles.section}>
                        <Checkbox
                            section
                            value={isLocation}
                            onValueChange={setIsLocation}
                            color={isLocation ? '#4a4a4a' : undefined}
                        />
                        <Text style={styles.labelText}>Location</Text>
                    </View>

                    <Text style={styles.labelText}>Asset Tags:</Text>
                    <View style={styles}>
                        {tagList.length > 0 && tagList.map((tag, index) => (
                            <View>
                                <TouchableOpacity
                                    style={[styles.tagButton]}
                                    key={tag}
                                    onPress={() => removeTag(tag)}>
                                    <Text style={styles.tagButtonText} key={tag}>[{tag}]</Text>
                                </TouchableOpacity>
                            </View>
                        ))}
                        {
                            tagList.length === 0 &&
                            <View style={styles.tagNA}>
                                <Text style={styles.tagButtonText}>N/A</Text>
                            </View>
                        }
                    </View>

                    {!photo &&
                        <TouchableOpacity
                            style={[styles.button]}
                            onPress={() => takePhoto()}>
                            <Text style={styles.buttonText}>
                                <Ionicons name="camera" size={20} color="#ffffff" /> Take Photo
                            </Text>
                        </TouchableOpacity>
                    }

                    {photo &&
                        <View>
                            <TouchableOpacity
                                style={[styles.button]}
                                onPress={() => setPhoto(null)}>
                                <Text style={styles.buttonText}>
                                    <Ionicons name="trash" size={20} color="#ffffff" /> Clear Photo
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }

                    <TouchableOpacity
                        style={[styles.button]}
                        onPress={() => scanId()}>
                        <Text style={styles.buttonText}>
                            <Ionicons name="qr-code" size={20} color="#ffffff" /> Scan Tag
                        </Text>
                    </TouchableOpacity>

                    {isNew &&
                        <TouchableOpacity
                            style={[styles.button]}
                            onPress={() => createThing()}>
                            <Text style={styles.buttonText}>
                                <Ionicons name="color-wand" size={20} color="#ffffff" /> Create Thing
                            </Text>
                        </TouchableOpacity>
                    }
                    {!isNew &&
                        <View>
                            <TouchableOpacity
                                style={[styles.buttonRed]}
                                onPress={() => deleteThing()}>
                                <Text style={styles.buttonText}>
                                    <Ionicons name="skull" size={20} color="#ffffff" /> Delete
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={[styles.button]}
                                onPress={() => updateThing()}>
                                <Text style={styles.buttonText}>
                                    <Ionicons name="save" size={20} color="#ffffff" /> Update Thing
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }
                </ScrollView>
            </View>}
        </View>
    );
}
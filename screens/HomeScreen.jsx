import * as React from 'react';
import { View, Text, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import * as Utils from '../utils';
import { styles } from '../style';

export function HomeScreen({ navigation, route }) {
    useFocusEffect(
        React.useCallback(() => {
            Utils.getConfig("server_location", "").then((text) => {
                if (text === null || text === "") {
                    Alert.alert("Error", "Connection settings are not set. Please configure this installation via 'Settings'.")
                }
            });
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [])
    );

    return (
        <View style={styles.container}>
            <ScrollView>
                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('move-thing')}>

                    <Text style={[styles.buttonText, styles.bigButton]}>
                        <Ionicons name="airplane" size={20} color="#ffffff" /> Move Thing
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('checkout-thing')}>

                    <Text style={[styles.buttonText]}>
                        <Ionicons name="receipt" size={20} color="#ffffff" /> Checkout Thing
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('find-thing')}>
                    <Text style={[styles.buttonText]}>
                        <Ionicons name="map" size={20} color="#ffffff" /> Find Thing
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('view-thing')}>
                    <Text style={[styles.buttonText]}>
                        <Ionicons name="analytics" size={20} color="#ffffff" /> Analyze Thing
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('edit-thing', { newItem: true })}>
                    <Text style={[styles.buttonText]}>
                        <Ionicons name="color-wand" size={20} color="#ffffff" /> New Thing
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('user-config')}>
                    <Text style={styles.buttonText}>
                        <Ionicons name="person" size={20} color="#ffffff" /> Me
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => navigation.navigate('server-config')}>
                    <Text style={styles.buttonText}>
                        <Ionicons name="settings-sharp" size={20} color="#ffffff" /> Settings
                    </Text>
                </TouchableOpacity>

            </ScrollView>
        </View>
    );
}
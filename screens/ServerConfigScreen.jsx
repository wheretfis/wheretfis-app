import * as React from 'react';
import { View, Text, TouchableOpacity, TextInput, Alert, ScrollView } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import * as Utils from '../utils';
import { styles } from '../style';

export function ServerConfigScreen({ navigation, route }) {
    const [serverLocation, setServerLocation] = React.useState("");
    const [userApiKey, setUserApiKey] = React.useState("");

    function SaveConfig() {
        Utils.setConfig("server_location", serverLocation);
        Utils.setConfig("api_key", userApiKey);
        Alert.alert("Success", "Configuration saved.")
    }

    function ScanConfig() {
        navigation.navigate('scanner', {
            callingScreen: "server-config",
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            if (route.params?.scanResult) {
                // This is called if scanResult is not null, and presumably we just got done with the scanner.
                var url = Linking.parse(route.params?.scanResult);
                
                var fqLocation = route.params?.scanResult.split("?")[0];
                setServerLocation(fqLocation);
                setUserApiKey(url.queryParams.api_key);
            }

            Utils.getConfig("server_location", "").then((text) => {
                if (serverLocation === "" && text !== null) {
                    setServerLocation(text);
                }
            });

            Utils.getConfig("api_key", "").then((text) => {
                if (userApiKey === "" && text !== null) {
                    setUserApiKey(text);
                }
            });

            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [route.params?.scanResult])
    );

    return (
        <View style={styles.container}>
            <ScrollView>
                <Text style={styles.labelText}>Server Location:</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={text => setServerLocation(text)}
                    value={serverLocation}
                    placeholder="http://wheretfis.yourserver.com"
                    keyboardType="url" />

                <Text style={styles.labelText}>API Key:</Text>
                <TextInput
                    secureTextEntry={true}
                    style={styles.input}
                    onChangeText={text => setUserApiKey(text)}
                    value={userApiKey} />

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => SaveConfig()}>
                    <Text style={styles.buttonText}>
                        <Ionicons name="save" size={20} color="#ffffff" /> Save
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => ScanConfig()}>
                    <Text style={styles.buttonText}>
                        <Ionicons name="qr-code" size={20} color="#ffffff" /> Autoconfig
                    </Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
}
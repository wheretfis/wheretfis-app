import * as React from 'react';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { StyleSheet, View, Text, TouchableOpacity, Vibration } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { styles } from '../style';

export function ScannerScreen({ navigation, route }) {
    const [hasPermission, setHasPermission] = React.useState(false);
    const [scanned, setScanned] = React.useState(false);
    const [showHomeButton, setShowHomeButton] = React.useState(true);
    const [allowNullScan, setAllowNullScan] = React.useState(false);

    React.useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');

            if (route.params?.cancelButton) {
                setShowHomeButton(true);
            }

            if (route.params?.nullScan) {
                setAllowNullScan(true);
            }
        })();
    }, []);

    function finishScan(data, gotoOverride=null) {
        var options= {
            name: route.params?.callingScreen ? route.params?.callingScreen : "home",
            params: { scanResult: data },
            merge: true,
        };
        if(gotoOverride){
            options.name = gotoOverride;
        }
        navigation.navigate(options);
    }

    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);

        // Give a short alert to the user to let them know the scan was done.
        Vibration.vibrate(250);

        // Pass and merge params back to previous screen
        finishScan(data);
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (


        <View style={styles.endContainer}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />

            <View style={styles.flexRow}>
                {allowNullScan && <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => finishScan("")}>

                    <Text style={[styles.buttonText]}>
                        <Ionicons name="eye-off" size={20} color="#ffffff" /> None
                    </Text>
                </TouchableOpacity>}

                {showHomeButton && <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => finishScan(null, "home")}>

                    <Text style={[styles.buttonText]}>
                        <Ionicons name="close" size={20} color="#ffffff" /> Cancel
                    </Text>
                </TouchableOpacity>}
            </View>
        </View >

    );
}
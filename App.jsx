import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScannerScreen } from './screens/ScannerScreen';
import { HomeScreen } from './screens/HomeScreen';
import { UserConfigScreen } from './screens/UserConfigScreen';
import { ServerConfigScreen } from './screens/ServerConfigScreen';
import { EditThingScreen } from './screens/EditThingScreen';
import { MoveThingScreen } from './screens/MoveThingScreen';
import { CheckoutThingScreen } from './screens/CheckoutThingScreen';
import { CameraScreen } from './screens/CameraScreen';
import { ViewThingScreen } from './screens/ViewThingsScreen';
import { FindThingScreen } from './screens/FindThingScreen';

const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            {/* <StatusBar style="auto" /> */}
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="home" component={HomeScreen} options={{ title: "Where TF is..." }} />
                <Stack.Screen name="user-config" component={UserConfigScreen} options={{ title: "Me" }} />
                <Stack.Screen name="server-config" component={ServerConfigScreen} options={{ title: "Settings" }} />
                <Stack.Screen name="scanner" component={ScannerScreen} options={{ title: "Scan Thing" }} />
                <Stack.Screen name="scanner-container" component={ScannerScreen} options={{ title: "Scan Container" }} />
                <Stack.Screen name="edit-thing" component={EditThingScreen} options={{ title: "Edit Thing" }} />
                <Stack.Screen name="move-thing" component={MoveThingScreen} options={{ title: "Move Thing" }} />
                <Stack.Screen name="checkout-thing" component={CheckoutThingScreen} options={{ title: "Checkout Thing" }} />
                <Stack.Screen name="camera" component={CameraScreen} options={{ title: "Take Photo" }} />
                <Stack.Screen name="view-thing" component={ViewThingScreen} options={{ title: "View Thing" }} />
                <Stack.Screen name="find-thing" component={FindThingScreen} options={{ title: "Find Thing" }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;

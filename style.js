import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    button: {
        flex: 1,
        backgroundColor: '#4a4a4a',
        padding: 30,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
    },
    bigButton: {
        padding: 60,
    },
    buttonRed: {
        flex: 1,
        backgroundColor: '#703d3d',
        padding: 30,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
    },
    buttonText: {
        fontSize: 20,
        color: '#ffffff',
    },
    labelText: {
        padding: 20,
        fontSize: 20,
        color: '#4a4a4a',
        fontWeight: 'bold'
    },
    input: {
        height: 35,
        borderColor: 'gray',
        borderWidth: 0.5,
        padding: 4,
        margin: 12,
        textAlignVertical: 'top',
    },
    largeInput: {
        height: 70,
        borderColor: 'gray',
        borderWidth: 0.5,
        padding: 4,
        margin: 12,
        textAlignVertical: 'top',
    },
    center: {
        flex: 1,
        justifyContent: 'center',
    },
    section: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    checkbox: {
        margin: 8,
    },
    endContainer: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    flexRow: {
        flexDirection: "row",
    },
    oneOneRatio: {
        width: "100%",
        paddingTop: "100%",
        position: "relative"
    },
    tag: {
        height: 35,
        padding: 4,
        margin: 12,
        textAlignVertical: 'top',
        fontSize: 16,
        color: '#000000',
    },
    tagButton: {
        flex: 1,
        padding: 15,
        marginHorizontal: "20%",
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        borderColor: 'gray',
        borderWidth: 0.5,
    },
    tagButtonText: {
        fontSize: 16,
        color: '#4a4a4a',
    },
    tagNA: {
        flex: 1,
        padding: 15,
        marginHorizontal: "20%",
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
    },
    textBig: {
        padding: 20,
        fontSize: 28,
        color: '#4a4a4a',
        fontWeight: 'bold'
    },
    textSmall: {
        padding: 20,
        fontSize: 20,
        color: '#4a4a4a',
    },
    buttonParent: {
        flex: 1,
        backgroundColor: '#ffffff',
        padding: 30,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
    },
    buttonParentText: {
        fontSize: 20,
        color: '#4a4a4a',
    },

});
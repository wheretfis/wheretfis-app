import * as SecureStore from 'expo-secure-store';
import { Alert } from 'react-native';

export async function setConfig(key, value) {
    await SecureStore.setItemAsync(key, value);
}

export async function getConfig(key, defaultValue) {
    let result = await SecureStore.getItemAsync(key);
    if (result === undefined) {
        return defaultValue;
    }
    return result;
}

export async function apiCall(path, method = "GET", data = {}) {
    var serverLocation = await getConfig("server_location", "");
    var apiKey = await getConfig("api_key", "");

    var url = new URL(path, serverLocation);

    var options = {
        method: method,
        headers: {
            Accept: 'application/json',
            "X-API-KEY": apiKey,
            "Content-type": "application/json",
        }
    };

    if (method != "GET" && data != {}) {
        options.body = JSON.stringify(data);
    }
    else if (method === "GET" && data != {}) {
        url = new URL(path + "?" + new URLSearchParams(data), serverLocation);
    }

    return fetch(url, options)
        .then((response) => {
            var res = response.json();
            if (response.status >= 200 && response.status <= 299 && response.status != 204) {
                return res;
            }
            else if (response.status == 204) {
                //Some special handling for 204s
                return {};
            }
            else if (response.status == 400) {
                res.then(json => {
                    Alert.alert("Error: Input", json.detail);
                })
            }
            else if (response.status == 401) {
                Alert.alert("Error: Forbidden", "Failed to validate user credentials. Make sure the API key given in Connection Settings is correct.");
            }
            else if (response.status == 403) {
                res.then(json => {
                    Alert.alert("Error: Unauthorized", json.detail);
                })
            }
            else if (response.status == 404) {
                res.then(json => {
                    Alert.alert("Error: Not Found", json.detail);
                })
            }
            else if (response.status == 422) {
                Alert.alert("Error: Validation", "Input given was rejected by the server. Please check the fields entered.");
            }
            else {
                Alert.alert("Error: Server", "The server reported an error while processing the request. Please check server logs.");
            }
        })
        .catch((error) => {
            Alert.alert("Error: Other", `The server at '${serverLocation}' failed to respond correctly. Please check your connection settings.\n\nDetail: ` + error)
            console.error(error);
        });
}
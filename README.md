# WhereTF App
This is the source code for the WhereTFis app for both iOS and Android. Built in [React Native](https://reactnative.dev/) and [Expo](https://docs.expo.dev/).

## Developer Setup
* https://docs.expo.dev/get-started/installation/
    * `npm install --global expo-cli`
* `cd wheretfis-app`
* `npm install` 
* `expo start`